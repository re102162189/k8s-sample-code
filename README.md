### Prerequirements
* docker (v20.10.17) [install](https://docs.docker.com/engine/install/)
* enable kubernetes (v1.25.0) in docker preference
* Test under macOS Monterey 12.6, CPU: 1.4GHz Interl Core i5 / RAM: 16GB 2133MHz LPDDR3

### Demo
* Creating a Spring Boot for replica set in namespace "appserver"
* Creating a MySQL for stateful set in namespace "database"
* app server: connecting db from different namespace

### pack spring boot & build image, then push image to docker hub  
```
$ cd springio-api
$ docker build -t springio:1.0.0 .

# this image tag and "image" (in k8s-app/deployment.yaml) should be identical
#
$ docker login
$ docker tag springio ${your_docker_hub}/springio:1.0.0
$ docker push ${your_docker_hub}/springio:1.0.0
```

### check k8s cluster status  
```
$ kubectl cluster-info
$ kubectl get nodes
$ kubectl get services
```

### apply all service (app & mysql)  
```
$ cd ..
$ kubectl apply -f base
$ kubectl apply -f database
$ kubectl apply -f appserver
```

### check service,pod status  
```
$ kubectl get svc,pods -n database
$ kubectl get svc,pods -n appserver
```

### or using kubernetes proxy
```
# open browser: http://localhost:8080/swagger-ui.html
#
$ kubectl port-forward -n appserver svc/springio 8080:8080
```

========================== reset ===============================
### delete all in namespace "springio" & PV  
```
$ kubectl delete namespaces database appserver
$ kubectl delete pv mysql-pv
```

========================== debug ===============================
### debug pod & check pod logs  
```
$ kubectl describe pods ${POD_NAME} -n appserver 
$ kubectl logs ${POD_NAME} -n appserver   
```

### look inside mysql  
```
$ kubectl -n appserver exec -it ${MYSQL_POD_NAME} -- mysql -u root -p
```

### if pods stuck in terminating status  
```
$ kubectl delete pod <PODNAME> --grace-period=0 --force -n appserver  
```